import 'package:flutter/material.dart';
import 'package:myapp/Registration.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Registration(),
      theme: ThemeData(
        primarySwatch: Colors.green,
        accentColor: Colors.blue,
        scaffoldBackgroundColor: Colors.white,
      ),
      color: Colors.amber,
    );
  }
}
