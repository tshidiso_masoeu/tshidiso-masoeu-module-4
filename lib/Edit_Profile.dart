import 'dart:html';

import 'package:flutter/material.dart';
import 'package:myapp/Dashboard.dart';
import 'package:myapp/Login.dart';

class Edit_Profile extends StatelessWidget {
  const Edit_Profile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //The Title

      appBar: AppBar(title: const Text("Edit Profile")),

      //The body of the app
      body: Center(
        child: EditProfile(),
      ),
      bottomNavigationBar: BottomAppBar(
        shape: const CircularNotchedRectangle(),
        child: Container(
          height: 50.0,
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => const Dashboard()),
          )
        },
        child: const Icon(Icons.home),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}

//Form Class

class EditProfile extends StatelessWidget {
  const EditProfile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      const Padding(
        padding: EdgeInsets.symmetric(horizontal: 4, vertical: 6),
        child: TextField(
            decoration: InputDecoration(
                border: OutlineInputBorder(), hintText: 'First Name')),
      ),
      Padding(
        padding: EdgeInsets.symmetric(horizontal: 4, vertical: 6),
        child: TextField(
            decoration: InputDecoration(
                border: OutlineInputBorder(), hintText: 'Last Name')),
      ),
      Padding(
        padding: EdgeInsets.symmetric(horizontal: 4, vertical: 6),
        child: TextField(
            decoration: InputDecoration(
                border: OutlineInputBorder(), hintText: 'Email')),
      ),
      Padding(
        padding: EdgeInsets.symmetric(horizontal: 4, vertical: 6),
        child: TextField(
            decoration: InputDecoration(
                border: OutlineInputBorder(), hintText: 'Contact Details')),
      ),
      Padding(
        padding: EdgeInsets.symmetric(horizontal: 4, vertical: 6),
        child: ElevatedButton(
            child: const Text('Update'),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const Dashboard()),
              );
            }),
      )
    ]);
  }
}
