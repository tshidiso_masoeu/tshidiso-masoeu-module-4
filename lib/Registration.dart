import 'package:flutter/material.dart';
import 'package:myapp/Login.dart';

class Registration extends StatelessWidget {
  const Registration({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //The Title

      appBar: AppBar(title: const Text("Registration")),

      //The body of the app
      body: Center(
        child: UserDetails(),
      ),
    );
  }
}

//Input Class

class UserDetails extends StatelessWidget {
  const UserDetails({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      const Padding(
        padding: EdgeInsets.symmetric(horizontal: 4, vertical: 6),
        child: TextField(
            decoration: InputDecoration(
                border: OutlineInputBorder(), hintText: 'First Name')),
      ),
      Padding(
        padding: EdgeInsets.symmetric(horizontal: 4, vertical: 6),
        child: TextField(
            decoration: InputDecoration(
                border: OutlineInputBorder(), hintText: 'Last Name')),
      ),
      Padding(
        padding: EdgeInsets.symmetric(horizontal: 4, vertical: 6),
        child: TextField(
            decoration: InputDecoration(
                border: OutlineInputBorder(), hintText: 'Email')),
      ),
      Padding(
        padding: EdgeInsets.symmetric(horizontal: 4, vertical: 6),
        child: TextField(
            decoration: InputDecoration(
                border: OutlineInputBorder(), hintText: 'Contact Details')),
      ),
      Padding(
        padding: EdgeInsets.symmetric(horizontal: 4, vertical: 6),
        child: TextField(
            decoration: InputDecoration(
                border: OutlineInputBorder(), hintText: 'Password')),
      ),
      Padding(
        padding: EdgeInsets.symmetric(horizontal: 4, vertical: 6),
        child: TextField(
            decoration: InputDecoration(
                border: OutlineInputBorder(), hintText: 'Confirm Password')),
      ),
      Padding(
        padding: EdgeInsets.symmetric(horizontal: 4, vertical: 6),
        child: ElevatedButton(
            child: const Text('Register'),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const Login()),
              );
            }),
      )
    ]);
  }
}
